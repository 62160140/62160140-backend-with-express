const express = require('express')
const { json } = require('express/lib/response')
const router = express.Router()

const products = [
  { id: 1, name: 'iPad gen9 64G Wifi', price: 10000 },
  { id: 2, name: 'iPad gen10 64G Wifi', price: 10005 },
  { id: 3, name: 'iPad gen11 64G Wifi', price: 10005 },
  { id: 4, name: 'iPad gen12 64G Wifi', price: 10006 },
  { id: 5, name: 'iPad gen5 64G Wifi', price: 10007 },
  { id: 6, name: 'iPad gen7 64G Wifi', price: 10008 },
  { id: 7, name: 'iPad gen8 64G Wifi', price: 10009 },
  { id: 8, name: 'iPad gen9 64G Wifi', price: 100010 },
  { id: 9, name: 'iPad gen10 64G Wifi', price: 1000111 },
  { id: 10, name: 'iPad gen11 64G Wifi', price: 1000555 }
]

// สำหรับกำหนด index ให้ newProduct
let lastid = 11

const getProduct = function (req, res, next) {
  const index = products.findIndex((item) => item.id === parseInt(req.params.id))
  if (index >= 0) {
    res.json(products[index])
  } else {
    res.status(404).json({
      error: 404
    })
  }
}

const getProducts = function (req, res, next) {
  res.json(products)
}

const addProducuts = function (req, res, next) {
  // debug
  console.log(req.body)

  // สร้าง object ตัวใหม่
  const newProduct = {
    id: lastid,
    name: req.body.name,
    price: parseFloat(req.body.price)
  }
  products.push(newProduct)
  // เพิ่มค่า indexid  หลัง push ไปแล้ว
  lastid++
  res.status(201).json(req.body)
}

const updateProducut = function (req, res, next) {
  const product = {
    id: parseInt(req.query.id),
    name: req.query.name,
    price: parseFloat(req.query.price)
  }
  const index = products.findIndex((item) => item.id === parseInt(req.params.id))
  if (index > -1) {
    /* ------------ update product ตำแหน่ง index ------------ */
    products[index] = product
    res.json(products[index])
  } else {
    res.status(404).json({
      error: 404
    })
  }
}

const deleteProducut = function (req, res, next) {
  const index = products.findIndex((item) => item.id === parseInt(req.params.id))
  if (index > -1) {
    /* ------------ ลบ product index ไป 1 อัน ------------ */
    products.splice(index, 1)
    res.status(204).send()
  } else {
    res.status(404).json({
      error: 404
    })
  }
}

/* ----------- ต่างกันที่ method get กับ post ----------- */
/* ------------------- get All Product ------------------ */
router.get('/', getProducts)
/* ------------------- get  Product id ------------------- */
router.get('/:id', getProduct)
/* --------------------- add Product -------------------- */
router.post('/', addProducuts)
/* --------------------- editProduct -------------------- */
router.put('/:id', updateProducut)
/* -------------------- deleteProduct ------------------- */
router.delete('/:id', deleteProducut)

module.exports = router
